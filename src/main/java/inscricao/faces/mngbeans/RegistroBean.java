/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import utfpr.faces.support.PageBean;

/**
 *
 * @author gab
 */
@ApplicationScoped
@ManagedBean(name="registroBean")
public class RegistroBean extends PageBean {
   public ArrayList<Candidato> arrayListCandidatos;
   
   
   public RegistroBean(){
       this.arrayListCandidatos = new ArrayList<Candidato>();
   }

    public ArrayList<Candidato> getArrayListCandidatos() {
        return arrayListCandidatos;
    }

    public void setArrayListCandidatos(ArrayList<Candidato> arrayListCandidatos) {
        this.arrayListCandidatos = arrayListCandidatos;
    }
   
}
